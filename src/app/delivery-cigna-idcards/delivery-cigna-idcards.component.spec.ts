import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryCignaIdcardsComponent } from './delivery-cigna-idcards.component';

describe('DeliveryCignaIdcardsComponent', () => {
  let component: DeliveryCignaIdcardsComponent;
  let fixture: ComponentFixture<DeliveryCignaIdcardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryCignaIdcardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryCignaIdcardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
