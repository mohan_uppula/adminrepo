import { Timestamp } from "rxjs/internal/operators/timestamp";

export interface ExternalUser {
    fullName: string;
    title: string;
    Address: string;
    floor: string;
    workPhoneNumber: string;
    workPhoneCountryCode: number;
    mobilePhoneNumber: string;
    mobilePhoneCountryCode: string;
    primaryContact: string;
    email: string;
    comments: string;
    contactType: string;
    companyName: string;
    isW360Access: boolean;
    isDomestic: string[];
    isInternational: string[];
}
export interface ExternalUserSaveModel extends ExternalUser {
    tenantName: string;
    PrimayEmailId: string;
    firstName: string;
    lastName: string;
    tenantDomain: string;
    tenantPermisionRequest: string[];
    legacyPermissionRoles: LegacyPermissionRoles;
    internal_user: boolean;
    password: string;
    Domestic: string[];
    International: string[];
    isDeleted: boolean;
}
export interface LegacyPermissionRoles {
    applicationId: number;
    applicationName: string;
    subApplicationId: number;
    roleId: string;
}
export interface ContactType {
    value: string;
    text: string;
}
export interface EventGridModel {
    id: string,
    eventType: string;
    subject: string;
    eventTime: Date;
    dataVersion: string;
    metadataVersion: number;
    data: ExternalUserSaveModel;
}
export interface ExternalUserElasticSearch {
    Values: ExternalUserSaveModel;
}

