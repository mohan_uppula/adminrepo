import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountActivityComponent } from './account-activity/account-activity.component';
import { AddBusinessContactComponent } from './add-business-contact/add-business-contact.component';
import { AddOpenItemComponent } from './add-open-item/add-open-item.component';
import { ClientComponent } from './client/client.component';
import { ClientViewComponent } from './client-view/client-view.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ContactsComponent } from './contacts/contacts.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DeliveryCignaIdcardsComponent } from './delivery-cigna-idcards/delivery-cigna-idcards.component';
import { DocumentsComponent } from './documents/documents.component';
import { ElasticSearchComponent } from './elastic-search/elastic-search.component';
import { GlobalComponent } from './global/global.component';
import { InsightsComponent } from './insights/insights.component';
import { ShowContactsComponent } from './show-contacts/show-contacts.component';
import { ExternalUserComponent } from './user/external-user/external-user.component';
import { InternalUserComponent } from './user/internal-user/internal-user.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from "@angular/material/toolbar"
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMenuModule } from '@angular/material/menu';
//import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppConfigService } from './core/config/app-config-service'; 
import { HomeComponent } from './modules/home/pages/home/home.component';


@NgModule({
  entryComponents: [
    ConfirmDialogComponent
  ],

  declarations: [
    AppComponent,
    AccountActivityComponent,
    AddBusinessContactComponent,
    AddOpenItemComponent,
    ClientComponent,
    ClientViewComponent,
    ConfirmDialogComponent,
    ContactsComponent,
    DashboardComponent,
    DeliveryCignaIdcardsComponent,
    DocumentsComponent,
    ElasticSearchComponent,
    GlobalComponent,
    InsightsComponent,
    ShowContactsComponent,
    ExternalUserComponent,
    InternalUserComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatMenuModule,
   // NgbModule,
    ToastrModule.forRoot()
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => {
        return () => {
          //Make sure to return a promise!
          return appConfigService.loadAppConfig()
        };
      }
    }
  ],
  bootstrap: [HomeComponent]
})
export class AppModule { }
