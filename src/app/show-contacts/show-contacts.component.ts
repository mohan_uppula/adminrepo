import { Component, Input, OnInit } from '@angular/core';
import { ContactType, ExternalUserSaveModel } from '../core/models/user-model';

@Component({
  selector: 'app-show-contacts',
  templateUrl: './show-contacts.component.html',
  styleUrls: ['./show-contacts.component.css']
})
export class ShowContactsComponent implements OnInit {

  @Input() serviceData = [] as ExternalUserSaveModel[];
  @Input() columns: string[] = [];
  

  contactTypes: ContactType[] = [
    { value: "Benefit Additional", text: "Additional" },
    { value: "Benefit C Suite", text: "C-Suite" },
    { value: "Benefit Primary", text: "Day-to-day" }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
