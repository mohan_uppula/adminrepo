import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  clientName:string = "";

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const data_received_homecomponent = this.route.snapshot.queryParams;

    //console.log(data_received_homecomponent);

    if (data_received_homecomponent.hasOwnProperty('clientName')) {

      this.clientName = atob(data_received_homecomponent['clientName'])
      
    }

  }

}
