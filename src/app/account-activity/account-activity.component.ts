import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonService } from '../core/common.service';

@Component({
  selector: 'app-account-activity',
  templateUrl: './account-activity.component.html',
  styleUrls: ['./account-activity.component.css']
})
export class AccountActivityComponent implements OnInit {

  constructor(private http: HttpClient,private DService:CommonService) { }

  blobName: string = "activitylogger"
  paginationSize: Number = 3
  page: Number = 0
  startNumber: Number = 0
  prevButton: boolean = false
  loading: boolean = false

  userActivities: any

  ngOnInit(): void {
    this.page = 0;
    this.getRecentActivityData();
  }

  getRecentActivityData() {
    this.loading = true;
    const json_body = '{"select_fields": ["id","event-type","Client_addedBy","eventTime","client_name"],"filter": {"Client_addedBy": "'+ atob(localStorage.getItem("temp_email") ?? "") +'"},"orderby" : ["id"],"asc": true}'
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    
    this.DService.getUserActivity(this.blobName,String(this.paginationSize),String(this.startNumber),json_body)

    //this.http.post('https://ms-dev-activitylogsfunctions.azurewebsites.net/api/woodruff/query/activitylogs/' + this.blobName + '?size=' + String(this.paginationSize) + '&page=' + String(this.startNumber), json_body, { headers: headers })
      .subscribe(res => {
        this.userActivities = res
        this.loading = false;
        //console.log(res)
      })
  }

  prev() {
    this.page = Number(this.page) - 1

    if (this.page == 0) 
      this.prevButton = false    
    else
      this.prevButton = true

    //console.log(Number(this.page) * Number(this.paginationSize))
    this.startNumber = Number(this.page) * Number(this.paginationSize)
    
    this.getRecentActivityData();
  }

  next() {

    this.page = Number(this.page) + 1    
      this.prevButton = true

    //console.log(Number(this.page) * Number(this.paginationSize))

    this.startNumber = Number(this.page) * Number(this.paginationSize)
    this.getRecentActivityData();

  }
}
