import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { forkJoin } from 'rxjs';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { ContactType, EventGridModel, ExternalUser, ExternalUserSaveModel } from 'src/app/models/external-user';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-internal-user',
  templateUrl: './internal-user.component.html',
  styleUrls: ['./internal-user.component.css']
})
export class InternalUserComponent implements OnInit {

  registerForm!: FormGroup;
  loading = false;
  submitted = false;
  error!: string;
  phoneNumber = "\\(?\\d{3}\\)?[. -]? *\\d{3}[. -]? *[. -]?\\d{4}";
  isEmailExist: boolean = false;
  queryParams: Params = {};
  isEditMode: boolean = false;
  employeeDetails = {} as ExternalUserSaveModel;
  selectedClientCompany: string = atob(localStorage.getItem("temp_companyName") ?? "")!;

  constructor(private fb: FormBuilder, private userService: UserService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<InternalUserComponent>,
    private removedialogRef: MatDialog,
    private notifyService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: ExternalUserSaveModel) { }

    contactTypes: ContactType[] = [
      { value: "Benefit Additional", text: "Additional" },
      { value: "Benefit C Suite", text: "C-Suite" },
      { value: "Benefit Primary", text: "Day-to-day" }
    ];

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      title: ['', Validators.required],
      Address: ['', Validators.required],
      floor: [''],
      workPhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
      workPhoneCountryCode: ['', Validators.required],
      mobilePhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
      mobilePhoneCountryCode: ['', Validators.required],
      morePhoneNumbers: this.fb.array([]),
      primaryContact: [''],
      email: ['', [Validators.required, Validators.email]],
      comments: ['', Validators.required],
      contactType: ['', Validators.required],
      isDomestic: [false],
      isInternational: [false],
      isW360Access: [false]
    });
    this.defaultValues();
    this.setControlState("disable");
    this.route.queryParams.subscribe(params => {
      this.queryParams = params;
      if (Object.keys(params).length > 0) {
        this.onEditMode();
        this.setControlState("email");
      }
    });
    if (this.data && this.data.email != "") {
      this.registerForm.patchValue(this.data);
      this.employeeDetails = this.data;
      this.isEditMode = true;
      this.setControlState("email");
    }
  }
  defaultValues() {
    this.registerForm.get("contactType")?.setValue("Benefit Primary");
    this.registerForm.get("workPhoneCountryCode")?.setValue("1");
    this.registerForm.get("mobilePhoneCountryCode")?.setValue("1");
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    this.loading = true;
    console.log(this.registerForm)
    const saveResult: ExternalUserSaveModel = Object.assign({}, this.registerForm.getRawValue());
    saveResult.PrimayEmailId = saveResult.email;
    saveResult.tenantName = saveResult.firstName + saveResult.lastName;
    saveResult.internal_user = true;
    saveResult.password = "welcome@123";
    saveResult.companyName = this.selectedClientCompany;

    saveResult.Domestic = this.employeeDetails.Domestic || [];
    saveResult.International = this.employeeDetails.International || [];

    this.setDomesticNonDomestic(saveResult);

    const eventGrid: EventGridModel = {
      data: saveResult,
      dataVersion: "1.0",
      eventTime: new Date(),
      eventType: "add_external_user_event",
      id: "2022",
      metadataVersion: 1,
      subject: "Register User",
    }
    let eventGridList = [] as EventGridModel[];
    eventGridList.push(eventGrid);
    console.log(eventGridList);

    forkJoin({
      //eventgrid: this.userService.registerEventGrid(eventGridList),
      elastic: this.userService.registerElasticSearch(saveResult)
    })
      .subscribe(
        (data) => {
          console.log(data);
          this.clearControls();
          this.defaultValues();
          this.loading = false;
          this.dialogRef.close({
            clicked: 'submit'
          });
        },
        error => {
          this.notifyService.showError("something went wrong", "Error..!");
          this.error = error;
          console.log(error);
          this.loading = false;
        });
  }
  setDomesticNonDomestic(result: ExternalUserSaveModel) {

    //check company exist for domestic or not
    //if domestic/international flag true push that element to respective array else pop from array
    if (this.registerForm.get("isDomestic")?.value) {
      result.Domestic?.indexOf(this.selectedClientCompany) === -1 ?
        result?.Domestic.push(this.selectedClientCompany) : console.log("already exist");
    }
    else if (!this.registerForm.get("isDomestic")?.value) {
      const index = result.Domestic.indexOf(this.selectedClientCompany);
      if (index !== -1) {
        result?.Domestic.splice(index, 1);
      }
    }

    if (this.registerForm.get("isInternational")?.value) {
      result.International?.indexOf(this.selectedClientCompany) === -1 ?
        result.International?.push(this.selectedClientCompany) : console.log("already exist");
    }
    else if (!this.registerForm.get("isInternational")?.value) {
      const index = result.International?.indexOf(this.selectedClientCompany);
      if (index !== -1) {
        result.International?.splice(index, 1);
      }
    }
  }
  onEditMode() {
    if (this.queryParams.hasOwnProperty('email')) {
      this.userService.GetUserFromElasticSearch(this.queryParams.email, true).subscribe(
        data => {
          const result: ExternalUserSaveModel = data.Values;
          this.registerForm.patchValue(result);
          if (result.Domestic.indexOf(this.selectedClientCompany) > -1) {
            this.registerForm.patchValue({
              isDomestic: 1
            });
          }
          if (result.International.indexOf(this.selectedClientCompany) > -1) {
            this.registerForm.patchValue({
              isInternational: 1
            });
          }
          this.isEditMode = true;
        },
        error => {
          console.log(error);
        });
    }
    console.log(this.queryParams);
  }
  selectionChange(controlName: string) {
    console.log(this.registerForm.get("workPhoneContactType")?.value);
    // if(this.registerForm.get("workPhoneContactType")?.value && !this.registerForm.get("workPhoneContactType")?.value)
    // {
    this.registerForm.get("workPhoneContactType")?.setValue("");
    // }
  }
  addPhoneNumber(event: Event) {
    event.preventDefault();
  }
  get morePhoneNumbers() {
    return this.registerForm.controls["morePhoneNumbers"] as FormArray;
  }

  addPhoneNumbers(event: Event) {
    event.preventDefault();
    // const phoneNumberForm = this.fb.group({
    //   workPhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
    //   workPhoneCountryCode: ['', Validators.required],
    // });
    // this.morePhoneNumbers.push(phoneNumberForm);
  }

  deletePhoneNumbers(phoneNumberIndex: number) {
    this.morePhoneNumbers.removeAt(phoneNumberIndex);
  }

  validateEmail() {
    if (!this.registerForm.get("email")?.valid) return;
    this.isEmailExist = false;
    const email: string = this.f.email.value;
    this.userService.GetUserFromElasticSearch(email, true).subscribe(
      data => {
        this.isEmailExist = true;
        if (Object.keys(data.Values).length > 0) {
          this.employeeDetails = data.Values;
          this.registerForm.patchValue(data.Values);
        }
        else {
          this.clearControls();
          this.registerForm.patchValue({ email: email });
          this.defaultValues();
        }
        this.setControlState("enable");
      },
      error => {
        console.log(error);
        this.error = error;
        this.loading = false;
        this.setControlState("enable");
        this.clearControls();
        this.registerForm.patchValue({ email: email });
        this.defaultValues();
      });
  }
  setControlState(status: string) {
    Object.keys(this.registerForm.controls).forEach(key => {
      if (status === "disable" && key !== "email") {
        this.registerForm.get(key)?.disable();
      }
      else if (status === "email" && key === "email") {
        this.registerForm.get(key)?.disable();
      }
      else {
        this.registerForm.get(key)?.enable();
      }
    });
  }
  clearControls() {
    var modal = <ExternalUser>{};
    this.registerForm.reset(modal);
  }
  getFormValidationErrors() {
    Object.keys(this.registerForm.controls).forEach(key => {
      const controlErrors: any = this.registerForm?.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
         console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }
  removeEmployee(event: Event) {
    event.preventDefault();
    const confirmDialog = this.removedialogRef.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm..!',
        message: `Are you sure, you want to remove this team member?`
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.userService.deleteClient(this.data.email, true, true).subscribe(
          data => {
            this.dialogRef.close({
              clicked: 'remove'
            });
          });
      }
    });

  }
}
