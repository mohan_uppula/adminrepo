import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { forkJoin } from 'rxjs';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { ContactType, EventGridModel, ExternalUser, ExternalUserSaveModel } from 'src/app/models/external-user';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-external-user',
  templateUrl: './external-user.component.html',
  styleUrls: ['./external-user.component.css']
})
export class ExternalUserComponent implements OnInit {

  registerForm!: FormGroup;
  loading = false;
  submitted = false;
  error!: string;
  phoneNumber = "\\(?\\d{3}\\)?[. -]? *\\d{3}[. -]? *[. -]?\\d{4}";
  isEmailExist: boolean = false;
  queryParams: Params = {};
  isEditMode: boolean = false;

  constructor(private fb: FormBuilder, private userService: UserService, private route: ActivatedRoute,
    public dialogRef: MatDialogRef<ExternalUserComponent>, private removedialogRef: MatDialog,
    private notifyService: NotificationService,
    @Inject(MAT_DIALOG_DATA) public data: ExternalUserSaveModel) { }

    contactTypes: ContactType[] = [
      { value: "Benefit Additional", text: "Additional" },
      { value: "Benefit C Suite", text: "C-Suite" },
      { value: "Benefit Primary", text: "Day-to-day" }
    ];
  

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      title: ['', Validators.required],
      companyName: ['', Validators.required],
      Address: ['', Validators.required],
      floor: [''],
      workPhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
      workPhoneCountryCode: ['', Validators.required],
      mobilePhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
      mobilePhoneCountryCode: ['', Validators.required],
      morePhoneNumbers: this.fb.array([]),
      primaryContact: [''],
      email: ['', [Validators.required, Validators.email]],
      comments: ['', Validators.required],
      contactType: ['', Validators.required],
      isW360Access: [false, Validators.required],
      isRemoved: [0]
    });
    this.defaultValues();

    this.route.queryParams.subscribe(params => {
      this.queryParams = params;
      if (Object.keys(params).length > 0 && this.queryParams.hasOwnProperty('email')) {
        this.onEditMode(this.queryParams.email);
      }
    });
    if (this.data && this.data.email != "") {
      this.registerForm.patchValue(this.data);
    }
  }
  defaultValues() {
    this.registerForm.get("contactType")?.setValue("Benefit Primary");
    this.registerForm.get("workPhoneCountryCode")?.setValue("1");
    this.registerForm.get("mobilePhoneCountryCode")?.setValue("1");
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    this.loading = true;
    console.log(this.registerForm)
    const saveResult: ExternalUserSaveModel = Object.assign({}, this.registerForm.value);
    saveResult.PrimayEmailId = saveResult.email;
    saveResult.tenantName = saveResult.firstName + saveResult.lastName;
    saveResult.internal_user = false;
    saveResult.password = "welcome@123";

    const eventGrid: EventGridModel = {
      data: saveResult,
      dataVersion: "1.0",
      eventTime: new Date(),
      eventType: "add_external_user_event",
      id: "2022",
      metadataVersion: 1,
      subject: "Register User",
    }
    let eventGridList: EventGridModel[] = [];
    eventGridList.push(eventGrid);
    console.log(eventGridList);

    if (this.isEmailExist) {
      this.notifyService.showWarning("email already exist", "warning..!");
      return;
    }
    forkJoin({
      //eventgrid: this.userService.registerEventGrid(eventGridList),
      elastic: this.userService.registerElasticSearch(saveResult)
    })
      .subscribe(
        (data) => {
          console.log(data);
          var modal = <ExternalUser>{};
          this.registerForm.reset(modal);
          this.defaultValues();
          this.loading = false;
          this.dialogRef.close({
            clicked: 'submit',
            clientData: saveResult
          });
        },
        error => {
          this.notifyService.showError("something went wrong", "error..!");
          this.error = error;
          this.loading = false;
        });
  }
  onEditMode(email: string) {
    this.loading = true;
    if (email) {
      this.userService.GetUserFromElasticSearch(email, false).subscribe(
        data => {
          this.registerForm.patchValue(data.Values);
          this.isEditMode = true;
          this.loading = false;
        },
        error => {
          this.loading = false;
          console.log(error);
        });
    }
    console.log(this.queryParams);
  }
  selectionChange(controlName: string) {
    console.log(this.registerForm.get("workPhoneContactType")?.value);
    // if(this.registerForm.get("workPhoneContactType")?.value && !this.registerForm.get("workPhoneContactType")?.value)
    // {
    this.registerForm.get("workPhoneContactType")?.setValue("");
    // }
  }
  addPhoneNumber(event: Event) {
    event.preventDefault();
  }
  get morePhoneNumbers() {
    return this.registerForm.controls["morePhoneNumbers"] as FormArray;
  }

  addPhoneNumbers(event: Event) {
    event.preventDefault();
    // const phoneNumberForm = this.fb.group({
    //   workPhoneNumber: ['', [Validators.required, Validators.pattern(this.phoneNumber)]],
    //   workPhoneCountryCode: ['', Validators.required],
    // });
    // this.morePhoneNumbers.push(phoneNumberForm);
  }

  deletePhoneNumbers(phoneNumberIndex: number) {
    this.morePhoneNumbers.removeAt(phoneNumberIndex);
  }

  validateEmail() {
    this.isEmailExist = false;
    const email: string = this.f.email.value;
    this.userService.GetUserFromElasticSearch(email, false).subscribe(
      data => {
        this.isEmailExist = true;
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }
  removeClient(event: Event) {
    event.preventDefault();
    const confirmDialog = this.removedialogRef.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm..!',
        message: `Are you sure, you want to remove this client?`
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.userService.deleteClient(this.data.email, true, false).subscribe(
          data => {
            this.dialogRef.close({
              clicked: 'remove'
            });
          });
      }
    });

  }
}
