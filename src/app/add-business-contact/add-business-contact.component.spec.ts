import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBusinessContactComponent } from './add-business-contact.component';

describe('AddBusinessContactComponent', () => {
  let component: AddBusinessContactComponent;
  let fixture: ComponentFixture<AddBusinessContactComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddBusinessContactComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBusinessContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
