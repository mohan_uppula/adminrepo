import { Component, OnInit } from '@angular/core';
import { BusinessContact } from '../core/models/user-model';
import { CommonService } from '../core/common.service';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-add-business-contact',
  templateUrl: './add-business-contact.component.html',
  styleUrls: ['./add-business-contact.component.css']
})
export class AddBusinessContactComponent implements OnInit {

  businessContact = {} as BusinessContact;

  client: string = ''
  existingClients: string = ''

  constructor(private CService: CommonService,
    private NService: NotificationService,) { }

  
  //

  ngOnInit(): void {
    console.log("addbusinesscontact.component");
  }

  saveBusinessContact() {

    const document_form: BusinessContact = Object.assign({}, this.businessContact);

    Object.assign(document_form, {
      "associated_clients": [this.client]
      , "createdBy": atob(localStorage.getItem("temp_email") ?? "")
      , "contacts_only": true
    })

     this.CService.addBusinessClients(document_form.clientEmail, false, document_form)
       .subscribe(res => {
        // this.NService.showSuccess(res['value']['Message'].toString(),""); 
         console.log("add contacts res line 39",res)
         this.businessContact.clientEmail= '';
         this.businessContact.company= '';
         this.businessContact.name= '';
         this.businessContact.phone= '';
         this.client= '';
       })
  }

  retriveData() {
    //console.log(this.businessContact.clientEmail);
    if (this.businessContact.clientEmail != "" && this.businessContact.clientEmail != undefined) {
      let body: any = {};
      body.clientEmail = this.businessContact.clientEmail;
      this.CService.getBusinessClients(false, body)
        .subscribe(res => {
          const reponse = JSON.stringify(res);
          console.log("initial", reponse)
          if (reponse != '[]') {
            const data = JSON.stringify(res);
            const parseJson = JSON.parse(data)
            this.existingClients = parseJson[0]['source']['associated_clients'];
            this.businessContact.company = parseJson[0]['source']['company'];
            this.businessContact.name = parseJson[0]['source']['name'];
            this.businessContact.phone = parseJson[0]['source']['phone'];
            this.client = parseJson[0]['source']['associated_clients'];
            console.log(this.existingClients)
          }
          else {
            
            this.businessContact.company = '';
            this.businessContact.name = '';
            this.businessContact.phone = '';
            this.client = '';
          }
        })



    }
  }
}
