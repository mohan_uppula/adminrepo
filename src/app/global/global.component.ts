import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CommonService } from '../core/common.service';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class GlobalComponent implements OnInit {

  constructor(private common:CommonService) { }

  ngOnInit(): void {
  }

}
