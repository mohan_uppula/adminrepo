import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'jquery';
import { CommonService } from '../core/common.service';

@Component({
  selector: 'app-elastic-search',
  templateUrl: './elastic-search.component.html',
  styleUrls: ['./elastic-search.component.css']
})
export class ElasticSearchComponent implements OnInit {

  elastic_data: any = {
    container: '',
    directurl: '',
    Country: '',
    Document_Category: '',
    email: '',
    Generic_Document_Name: ''
  }

  response_data: any = [];

  ws_search: boolean = true;
  ws_search_result: boolean = false;

  searchData: string = ""
  email: any = localStorage.getItem("temp_email");
  index: string = "ms-document-metadata"
  loader: boolean = false;

  //search:string="woodruffmoonshot"; 

  constructor(private http: HttpClient, private router: Router,
    private CService: CommonService
  ) { }

  getData() {

    //console.log(this.searchData)

    const json_body = '{"SearchKey":"' + this.searchData + '"}'

    //Calling API
    let headers = new HttpHeaders();

    headers = headers.set('Content-Type', 'application/json');

    this.loader = true;
    this.CService.getSearchData(this.index, json_body)
      //this.http.post('https://wsms-dev-es-function-app.azurewebsites.net/query/'+ this.index, json_body ,{headers: headers})

      .subscribe(res => {
        this.response_data = res;
        this.ws_search = false;
        this.ws_search_result = true;
        this.loader = false;
        //this.elastic_data = res;
        //console.log(res)
        // console.log(JSON.parse( JSON.stringify(res)))

      })
  }


  updateData(Generic_Document_Name: any, Specific_Document_Name: any, Coverage_Type: any, Client: any) {

    const uniqueID = Generic_Document_Name + Specific_Document_Name + Coverage_Type + Client
    this.loader = true;
    this.CService.getDataToElasticSearch(uniqueID)
      //this.http.get('https://wsms-dev-es-function-app.azurewebsites.net/ms-document-metadata/documents/'+ uniqueID)

      .subscribe(res => {
        this.loader = false;
        this.router.navigate([`/documents`], { queryParams: { data: JSON.stringify(res) } });
      })


  }

  ngOnInit(): void {
    console.log("elastic-search");

  }

}
