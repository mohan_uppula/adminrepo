import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountActivityComponent } from 'src/app/account-activity/account-activity.component';
import { AddBusinessContactComponent } from 'src/app/add-business-contact/add-business-contact.component';
import { ClientViewComponent } from 'src/app/client-view/client-view.component';
import { ClientComponent } from 'src/app/client/client.component';
import { ContactsComponent } from 'src/app/contacts/contacts.component';
import { LoggedInGuardGuard } from 'src/app/core/guards/logged-in-guard.guard';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { DocumentsComponent } from 'src/app/documents/documents.component';
import { ElasticSearchComponent } from 'src/app/elastic-search/elastic-search.component';
import { GlobalComponent } from 'src/app/global/global.component';
import { InsightsComponent } from 'src/app/insights/insights.component';
import { HomeComponent } from './pages/home/home.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';

const routes: Routes = [
  {path:'nav',canActivate:[LoggedInGuardGuard],pathMatch:'full',component:HomeComponent},
  {path:'landingPage/:token',component:LandingPageComponent},
  {path:'documents',canActivate:[LoggedInGuardGuard] ,component: DocumentsComponent},
  {path:'search',canActivate:[LoggedInGuardGuard] ,component: ElasticSearchComponent},
  {path:'global',canActivate:[LoggedInGuardGuard] ,component: GlobalComponent}, 
  {path:'contacts',canActivate:[LoggedInGuardGuard] ,component: ContactsComponent},  
  {path:'home',canActivate:[LoggedInGuardGuard] ,component: DashboardComponent},  
  {path:'insights',canActivate:[LoggedInGuardGuard] ,component: InsightsComponent},
  {path:'client',canActivate:[LoggedInGuardGuard] ,component: ClientComponent},  
  {path:'activity',canActivate:[LoggedInGuardGuard] ,component: AccountActivityComponent},  
  {path:'clientview',canActivate:[LoggedInGuardGuard] ,component: ClientViewComponent},  
  {path:'addBusinessContact',canActivate:[LoggedInGuardGuard],component:AddBusinessContactComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
