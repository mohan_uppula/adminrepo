import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
import { Router } from '@angular/router';
import { ConstantPool } from '@angular/compiler';
import { CommonService } from 'src/app/core/common.service';
import { NotificationService } from 'src/app/services/notification.service';
import { BusinessContact } from 'src/app/core/models/user-model'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent {
  cliApp_toster: boolean = false;
  @ViewChild('sidenav') sidenav: any;
  isExpanded = true;
  sLogo = true;
  showSubmenu: boolean = false;
  showSubmenu1: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  isChecked: any;
  keycontacts: any;
  AddClient_select: boolean = true;
  AddClient_input: boolean = false;
  clientName: any = "";
  eventGridName: string = "ws-dev-moonshot-epicdatachangeeventgrid";
  username: string = "";
  shortName: string = "";
  clientNames: any;
  blobName: string = "activitylogger"
  companyName: string = ''
  businessContact: any;
  email: string = "";

  mouseenter() {
    // if (!this.isExpanded) {
    //   this.isShowing = true;
    //   this.sLogo= true;
    // }
  }
  mouseleave() {
    // if (!this.isExpanded) {
    //   this.isShowing = false;
    //   this.sLogo=false
    // }
  }
  tosterClose() { this.cliApp_toster = false; }


  helper = new JwtHelperService();

  constructor(private http: HttpClient, private edservice: CommonService, private route: Router, private NService: NotificationService) { }
// staticurl='https://woodruff-dev-function-app.azurewebsites.net/internal_users/documents/';

async ngOnInit(): Promise<void> {
  //----- user_identity_get_by_emailid_function_url from CliApp: MOONBEN-275---------- //  
  var url = this.edservice.getAPPCli();
  localStorage.setItem("BaseApp", btoa(url.BaseApp));
  localStorage.setItem("Benefits", btoa(url.Benefits));
  localStorage.setItem("Commercial", btoa(url.Commercial));
  const token: any = atob(localStorage.getItem('token') ?? "");
  //console.log("Hometoken", atob(token));         
  const splitToken = token.split(".");
  const decodeTokenAndGetUserDetails = (JSON.parse(atob(splitToken[1])));
  this.username = decodeTokenAndGetUserDetails["name"]

  this.shortName = decodeTokenAndGetUserDetails["name"].split(" ")
    .map((n: any[]) => n[0])
    .join("");
  this.shortName = this.shortName.substring(0, 2);
  debugger;

  this.email = decodeTokenAndGetUserDetails["email"] ?? decodeTokenAndGetUserDetails["upn"];

  if (this.email == undefined || this.email == "") {
    this.email = decodeTokenAndGetUserDetails["preferred_username"]
    localStorage.setItem('mail', btoa(this.email))

  }
  if (decodeTokenAndGetUserDetails["upn"] != "" && decodeTokenAndGetUserDetails["upn"] != undefined) {
    let res = await this.edservice.getUserEmail(decodeTokenAndGetUserDetails["upn"]);
    console.log(" checking res",res);
    // this.email = res['mail'];
    //localStorage.setItem('mail', btoa(res['mail']))
  }


  const token_Email = this.email;
  console.log("tE", this.email)

  const intgrate_url = url.ElasticsearchGetData.replace("{document_group}", "internal_users").replace("{document_type}", "documents").replace("{document_unique_id}", atob(localStorage.getItem('mail') ?? ""))
  // this.http.get(intgrate_url)
  var data = await this.edservice.getUserDetails(intgrate_url)

  console.log("data",data);
  if (data.hasOwnProperty('error')) {

    //this.NService.showError("there is no records with this " + token_Email + ' email',"");
    console.log("there is no records with this " + atob(localStorage.getItem('mail') ?? "") + ' email')
  }
  else {
    // .subscribe((data: any) => {
  /*  let UsersData = data['Values'];
    for (let value in UsersData) {
      localStorage.setItem('temp_' + value, btoa(UsersData[value]));
      //console.log('temp_' + value, UsersData[value])
    }
    for (let Svalue in UsersData.identity_details) {
      localStorage.setItem('temp_' + Svalue, btoa(UsersData.identity_details[Svalue]))
      //console.log('temp_' + Svalue, UsersData.identity_details[Svalue])
    }*/
  }
  // }, (error: any) => {
  //   //this.NService.showError("there is no records with this " + token_Email + ' email',"");
  //   console.log("there is no records with this " + atob(localStorage.getItem('mail') ?? "") + ' email')
  // // });


  let body: any = {};
  body.associated_clients = [];
  body.associated_clients.push(atob(localStorage.getItem("temp_companyName") ?? ""))
  body.contacts_only = "true"
  this.edservice.getBusinessClients(false, body)
    .subscribe(res => {
      const data = JSON.stringify(res);
      this.businessContact = JSON.parse(data);
      // console.log("business", this.businessContact)
    })

  this.companyName = atob(localStorage.getItem("temp_companyName") ?? "");
  this.edservice.getServiceTeam(this.companyName, true)
    .subscribe(
      data => {
        // console.log(data)
        this.keycontacts = data;
      },
      error => {
        console.log(error);
      });
  this.getClientNames()
}


AddClient($event: any) {
  // console.log($event.target.value);
  if ($event.target.value == "addClient") {
    this.AddClient_select = false;
    this.AddClient_input = true;
  }
}

addnewClient() {
  //console.log(this.clientName)
  if (this.clientName == '') {
    this.NService.showError("Client Name Required", "");
    // alert("Please enter userName");
    return;
  }
  else {
    this.callClientsEventGrid(this.clientName)
  }
}

callClientsEventGrid(client: any) {

  const jsondata = '{"Client_addedBy": "' + atob(localStorage.getItem("temp_email") ?? "") + '","event-type":"client","client_name":"' + client + '"}'
  this.edservice.logClients(this.eventGridName, jsondata)
    .subscribe(
      data => {

        this.AddClient_select = true;
        this.AddClient_input = false;
        this.route.navigate([`/client`], { queryParams: { clientName: btoa(client) } });
      },
      error => {
        console.log(error);
      });
}

redirectToContactsPage() {
  this.route.navigate(['/contacts'])
}

onChange($event: any) {
  if ($event.value == 'International') {
    this.route.navigate(['/global'])
  }
  if ($event.value == 'Domestic') {
    this.route.navigate(['/home']);
  }
}
getClientNames() {
  const json_body = '{"filters":"client_name" }'

  this.edservice.getClients('ms-document-metadata', json_body)
    .subscribe(res => {
      console.log('clients', res);

      //this.clientNames = res['aggregations']['item']['buckets'];
      // let data = JSON.stringify(res)
      // data = JSON.parse(data);
      // var uniqueZipCodes = '[';
      // for (let i = 0; i < data.length; i++) {

      //   if (data[i]['client_name'] != 'null' && data[i]['client_name'] != null) {
      //     if (uniqueZipCodes.indexOf(data[i]['client_name']) === -1) {

      //       uniqueZipCodes += '{"client_name" :"' + data[i]['client_name'] + '"},';
      //     }
      //   }
      // }
      // console.log(uniqueZipCodes.slice(0, -1) + ']');
      // //console.log(uniqueZipCodes.slice(0,-1)+ ']');
      //this.clientNames = JSON.parse(uniqueZipCodes.slice(0, -1) + ']')

    })
}


logout() {
  console.log("okay");

  for (var i = 0; i < localStorage.length; i++) {
    console.log(localStorage.getItem(localStorage[i].value));
    // if (localStorage.key(i).substring(0,3) == 'tmp_') { }
  }
}
}

