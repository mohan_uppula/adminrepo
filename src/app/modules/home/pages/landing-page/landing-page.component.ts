import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
  
    if (this.tokenExpired(this.route.snapshot.params.token)) {
      // token expired
      console.log("token expired");
      alert("token expired");
    } else {
      // token valid

            const token = btoa(this.route?.snapshot?.params?.token || "");
            if (localStorage.getItem(token) === null) {
                localStorage.setItem('token', token);
            }
            this.router.navigate(['/']);
        }
    }




    

  private tokenExpired(token: string) {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }
}
