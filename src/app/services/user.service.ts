import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EventGridModel, ExternalUser, ExternalUserElasticSearch, ExternalUserSaveModel } from '../models/external-user';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  getAll() {
    return this.http.get<ExternalUser[]>('api/currencies').pipe(
        map((res: ExternalUser[]) => res)
    );
}
registerEventGrid(eventGridList: EventGridModel[]) {
    return this.http.post(`https://eventgriduserpublisherfunction.azurewebsites.net/runtime/webhooks/EventGrid?functionName=RegisterUser&code=T9oq7d1638NRAYwjpUnLgQ1/gsEJ6XCr64O6cJVRHzVMZ5YCgVUdbg==`, eventGridList, { headers: { "aeg-event-type": "notification" } });
}

registerElasticSearch(externalUserSaveModel: ExternalUserSaveModel) {
    const documentName: string = externalUserSaveModel.internal_user ? "internal_users" : "external_users";
    const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${externalUserSaveModel.email}`;
    console.log(url);
    return this.http.post(url, externalUserSaveModel);
}
GetUserFromElasticSearch(email: string, isInternal: boolean) {
    const documentName: string = isInternal ? "internal_users" : "external_users";
    const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${email}`;
    console.log(url);
    return this.http.get<ExternalUserElasticSearch>(url).pipe(
        map((res: ExternalUserElasticSearch) => res)
    );
}
deleteClient(email: string, isDeleted: boolean, isInternal: boolean) {
    const documentName: string = isInternal ? "internal_users" : "external_users";
    const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${email}`;
    let body: any = {};
    body.isDeleted = isDeleted;
    return this.http.post(url, body);
}
}