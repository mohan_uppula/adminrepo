import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOpenItemComponent } from './add-open-item.component';

describe('AddOpenItemComponent', () => {
  let component: AddOpenItemComponent;
  let fixture: ComponentFixture<AddOpenItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOpenItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOpenItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
