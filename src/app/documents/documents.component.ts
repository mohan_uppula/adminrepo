import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { convertActionBinding } from '@angular/compiler/src/compiler_util/expression_converter';
import { NotificationService } from '../services/notification.service';
import { CommonService } from '../core/common.service';
import { postModel, postModel_update } from './form_model';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent {
  myForm = new FormGroup({
    
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.nullValidator]),
    fileName: new FormControl('', [Validators.nullValidator]),
    Geography: new FormControl('', [Validators.required]),
    Country: new FormControl('', [Validators.required]),
    Document_Category: new FormControl('', [Validators.nullValidator]),
    Generic_Document_Name: new FormControl('', [Validators.required]),
    Specific_Document_Name: new FormControl('', [Validators.required]),
    Format: new FormControl('', [Validators.nullValidator]),
    Line_of_Business: new FormControl('', [Validators.nullValidator]),
    Business_Level: new FormControl('', [Validators.nullValidator]),
    Coverage_Type: new FormControl('', [Validators.required]),
    Carrier: new FormControl('', [Validators.nullValidator]),
    Plan: new FormControl('', [Validators.nullValidator]),
    Plan_Type: new FormControl('', [Validators.nullValidator]),
    Plan_Start: new FormControl('', [Validators.nullValidator]),
    Search_Tags: new FormControl('', [Validators.nullValidator]),
    Internal_Information_Tags: new FormControl('', [Validators.nullValidator]),
    Optional_Comments_and_Tags: new FormControl('', [Validators.nullValidator]),
    Visibility: new FormControl('', [Validators.nullValidator]),
    Client: new FormControl('', [Validators.required]),
    Region_Selector: new FormControl('', [Validators.nullValidator]),
    Class_Selector: new FormControl('', [Validators.nullValidator]),
    Who_Uploaded_or_Saved: new FormControl('', [Validators.nullValidator]),
    Date_Uploaded_or_Saved: new FormControl('', [Validators.nullValidator]),
    Upload_Type: new FormControl('', [Validators.nullValidator]),
    Defined_in_systems_architecture: new FormControl('', [Validators.nullValidator]),
    second_Display: new FormControl('', [Validators.nullValidator]),
    Original_Document_Name: new FormControl('', [Validators.nullValidator]),
    Display_Name_or_Display_Location: new FormControl('', [Validators.nullValidator]),
    Path: new FormControl('', [Validators.nullValidator])

  });

  json_response: string = '';

  constructor(
    private http: HttpClient, 
    private router: Router, 
    private route: ActivatedRoute,
    private NService: NotificationService,
    private DService: CommonService
  ) { }

  get f() {
    return this.myForm.controls;
  }

  path: string = "dev-woodruffangular";
  target_container: string = "dev-angular"
  document_unique_id: string = "";
  email:any = localStorage.getItem("temp_email");
  index: string = "dev-woodruffmoonshot";
  loading:boolean = false;

  onFileChange(event : any) {

    const file = event.target.files[0];

    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      
      this.myForm.patchValue({
        fileSource: file,
        fileName: file.name
      });      
    }
  }

  submit() {

    const fileFormat = this.myForm.get('fileSource')?.value;  
    
    
    //Calling API
    let headers = new HttpHeaders();
    headers = headers.set('path', this.path);
    headers = headers.set('target_name', this.myForm.get('fileName')?.value);

    this.loading = true;
    this.DService.uploadDocumentToBlob(this.target_container, fileFormat, this.path,this.myForm.get('fileName')?.value)

    //this.http.post('https://wsmsblobdata-dev-function-app.azurewebsites.net/api/upload_item/target_container/' + this.target_container, fileFormat, { headers: headers })

      .subscribe(res => {

        this.json_response = JSON.stringify(res);        
        this.loading = false;
        
        //Calling Second API
        this.saveDataToElasticSearch();
        
       
      })
  }


  saveDataToElasticSearch() {
    
    this.myForm.patchValue({
          fileSource: '',
          fileName: '',
          file :''
        });

   //const document_form: postModel = Object.assign({}, this.myForm.value);
   const document_form: postModel = Object.assign({}, this.myForm.value);
   
    const firstApiResponse = JSON.parse(this.json_response);

    //Concatenating First API response with Form data
    Object.assign(document_form,{"container":firstApiResponse['container']},{"directurl":firstApiResponse['directurl']},{"modifiedBy":this.email})
    
    this.document_unique_id = document_form.Generic_Document_Name + document_form.Specific_Document_Name + document_form.Coverage_Type + document_form.Client

    let HTTPOptions: Object = {

      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      responseType: 'json'
    }    

    this.loading = true;
    this.DService.saveDataToElasticSearch(this.document_unique_id,document_form)

     //this.http.post('https://wsms-dev-es-function-app.azurewebsites.net/ms-document-metadata/documents/' + this.document_unique_id, test_form, HTTPOptions)

      .subscribe(res => {
       console.log(res);
        //this.NService.showSuccess(res['value']['Message'].toString(),"");      
        //alert(res['value']['Message'].toString());
        
        this.myForm.reset()
        this.loading = false;

      })
  }
  
  resetForm(){

    //console.log("Test")
    this.myForm.reset()
    
      
  }

  ngOnInit(): void {
    console.log("documents.component");
    const data_received_searchcomponent = this.route.snapshot.queryParams;

    if (data_received_searchcomponent.hasOwnProperty('data')) {

      const reparse_json: postModel_update = JSON.parse(data_received_searchcomponent['data']);

      //Binding values to Form
      this.myForm.patchValue(reparse_json.Values);
      
    }
    
  }
}
