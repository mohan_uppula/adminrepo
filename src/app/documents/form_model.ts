export interface postModel{

    Geography:string;
    Country:string;
     Document_Category:string;
     Generic_Document_Name:string;
     Specific_Document_Name:string;
     Format:string;
     Line_of_Business: string;
     Business_Level: string;
     Coverage_Type: string;
     Carrier: string;
     Plan: string;
     Plan_Type:string; 
     Plan_Start: string;
     Search_Tags: string;
     Internal_Information_Tags:string; 
     Optional_Comments_and_Tags: string;
     Visibility: string;
     Client: string;
     Region_Selector: string;
     Class_Selector: string;
     Who_Uploaded_or_Saved:string; 
     Date_Uploaded_or_Saved: string;
     Upload_Type: string;
     Defined_in_systems_architecture:string; 
     second_Display: string;
     Original_Document_Name:string; 
     Display_Name_or_Display_Location: string;
     path:string;
     container:string;
     directurl:string;     
     modifiedBy:string;
     modifieddate:Date;
  
  }

  export interface postModel_update{
  
    Values:postModel
}