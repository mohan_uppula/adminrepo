import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AddOpenItemComponent } from '../add-open-item/add-open-item.component';
import { DeliveryCignaIdcardsComponent } from '../delivery-cigna-idcards/delivery-cigna-idcards.component';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ClientViewComponent implements OnInit {
  step = 0;
  constructor(private dialogRef: MatDialog) { }

  ngOnInit(): void {
  }

  addopenItem(){
    //const dialogRef = this.dialogRef.open(AddOpenItemComponent, {
    const dialogRef = this.dialogRef.open(DeliveryCignaIdcardsComponent, {
      height: '100%',
      width: '50%',
      position: { right: '0' },
      panelClass: ['animate__animated','animate__slideInLeft'],
      disableClose: true
    });
  }

}
