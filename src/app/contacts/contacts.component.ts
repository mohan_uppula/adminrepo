import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatTabChangeEvent } from '@angular/material/tabs/tab-group';
import { interval } from 'rxjs';
import { CommonService } from '../core/common.service';
import { ContactType, ExternalUserSaveModel } from '../core/models/user-model';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ExternalUserComponent } from '../user/external-user/external-user.component';
import { NotificationService } from '../services/notification.service';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { InternalUserComponent } from '../user/internal-user/internal-user.component';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ContactsComponent implements OnInit {
  clientData = [] as ExternalUserSaveModel[];
  Profiledata: any;
  serviceData = [] as ExternalUserSaveModel[];
  domesticServiceData = [] as ExternalUserSaveModel[];
  internationalServiceData = [] as ExternalUserSaveModel[];
  constructor(private common: CommonService, private dialogRef: MatDialog,
    private notifyService: NotificationService,
    private http: HttpClient) { }

    contactTypes: ContactType[] = [
      { value: "Benefit Additional", text: "Additional" },
      { value: "Benefit C Suite", text: "C-Suite" },
      { value: "Benefit Primary", text: "Day-to-day" }
    ];
  
    serviceColumnsData: string[] = ["Name","Contact Info","Key Contact", "Role for Client"];
    clientColumnsData: string[] = ["Name","Contact Info","Contact Type", "360 Access","Last Access","Action"];
  
    loading: boolean = false;

  ngOnInit(): void {
   //this.getClients();
    //this.getServiceTeam();
    this.getProfile();
  }
  tabClick(event: MatTabChangeEvent) {
    this.loading = true;
    switch (event.tab.textLabel) {
      case "Client Contacts":
        this.getClients();
        break;
      case "Service Team":
        this.getServiceTeam();
        break;
      case "Client Profile":
        this.getProfile();
        break;
    }

  }
  removeClient(client: ExternalUserSaveModel) {
    const confirmDialog = this.dialogRef.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm..!',
        message: `Are you sure, you want to remove an client: ${client?.firstName} ${client?.lastName}?`
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.notifyService.showSuccess("client deleted successfully", "success");
        this.common.deleteClient(client.email, true, false).subscribe(
          data => {
            this.clientData = this.clientData.filter(item => item.email !== client.email);
          });
        //this.employeeList = this.employeeList.filter(item => item.employeeId !== employeeObj.employeeId);
      }
    });
  }
  addClient() {
    const dialogRef = this.dialogRef.open(ExternalUserComponent, {
      height: '100%',
      width: '50%',
      position: { right: '0' },
      panelClass: ['animate__animated','animate__slideInRight'],
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (data.clicked === 'submit') {
        this.clientData.push(data.clientData);
        this.notifyService.showSuccess("client added succesfully", "success");
      }
    });
  }
  editClient(client: ExternalUserSaveModel) {
    const dialogRef = this.dialogRef.open(ExternalUserComponent, {
      height: '100%',
      width: '50%',
      position: { right: '0' },
      data: client
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (data.clicked === 'submit') {
        this.notifyService.showSuccess("client updated succesfully", "success");
        let itemIndex = this.clientData.findIndex(item => item.email == client.email);
        this.clientData[itemIndex] = data.clientData;
      }
      else if (data.clicked === 'remove') {
        this.notifyService.showSuccess("client removed succesfully", "success");
        this.getClients();
      }
    });
  }
  addEmployee() {
    const dialogRef = this.dialogRef.open(InternalUserComponent, {
      height: '100%',
      width: '50%',
      position: { right: '0', top: '10' },
      panelClass: ['animate__animated','animate__slideInLeft'],
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (data.clicked === 'submit') {
        this.notifyService.showSuccess("employee added succesfully", "success");
        this.getServiceTeam();
      }
    });
    //window.location.replace(localStorage.getItem("BaseApp") + '/#/createinternaluser')
  }
  editEmployee(employee: ExternalUserSaveModel) {
    const dialogRef = this.dialogRef.open(InternalUserComponent, {
      height: '100%',
      width: '50%',
      position: { right: '0' },
      data: employee
    });
    dialogRef.afterClosed().subscribe((data) => {
      if (data.clicked === 'submit') {
        this.notifyService.showSuccess("employee updated succesfully", "success");
        this.getServiceTeam();
      }
      else if (data.clicked === 'remove') {
        this.notifyService.showSuccess("client removed succesfully", "success");
        this.getServiceTeam();
      }
      
    });
  }
  removeDomesticOrInternational(employee: ExternalUserSaveModel, type: string) {
    const confirmDialog = this.dialogRef.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm..!',
        message: `Are you sure, you want to remove an service team member: ${employee?.firstName} ${employee?.lastName}?`
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        let body = type == "D" ? {"isDomestic" : false } :  {"isInternational" : false };
        this.common.updateClient(employee.email, body, true).subscribe(
          data => {
            this.notifyService.showSuccess("employee deleted successfully", "success");
            const index = this.serviceData.findIndex(item => item.email === employee.email);
            
            if('D'){
              this.serviceData[index].isDomestic = false;
            }
            else{
              this.serviceData[index].isInternational = false;
            }
            this.divideDomesticInternational();
          });
        //this.employeeList = this.employeeList.filter(item => item.employeeId !== employeeObj.employeeId);
      }
    });
  }
  //localstorage with "cache_" prefix  along with expiry(24 hrs) time 
  cahche_set() {
    this.common.localCacheSession('test', 123456);
  }

  getProfile() {
    const companyName = atob(localStorage.getItem("temp_companyName") ?? "");
    //const companyName="uber";
    this.common.getprofile(companyName)
      .subscribe(
        data => {

          this.Profiledata = data;
          console.log(this.Profiledata);
        },
        error => {
          console.log(error);
        });

  }
  getClients() {
    this.loading = true;
    const companyName = atob(localStorage.getItem("temp_companyName") ?? "");
    this.common.getclient(companyName, false)
      .subscribe(
        data => {
          this.loading = false;
          this.clientData = data.filter(client => !client.isDeleted);
        },
        error => {
          this.loading = false;
          console.log(error);
        });
  }
  getServiceTeam() {
    this.loading = true;
    const companyName = atob(localStorage.getItem("temp_companyName") ?? "");
    this.common.getServiceTeam(companyName, true)
      .subscribe(
        data => {
          console.log(data);
          this.serviceData = data.filter(item => !item.isDeleted);
          this.loading = false;
          this.divideDomesticInternational();
        },
        error => {
          this.loading = false;
          console.log(error);
        });
  }
  divideDomesticInternational(){
    this.domesticServiceData = this.serviceData.filter(item => item.isDomestic);
    this.internationalServiceData = this.serviceData.filter(item => item.isInternational);
  }
}


