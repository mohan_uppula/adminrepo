import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
// import * as CryptoJS from 'crypto-js';
import { interval } from 'rxjs';
import { EmailValidator } from '@angular/forms';
import { BusinessContact, ExternalUserElasticQuerySearch, ExternalUserElasticSearch, ExternalUserSaveModel } from './models/user-model';
import { AppConfigService } from './config/app-config-service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
 //secretKey = "Woodruff";
 constructor(private http: HttpClient, private appConfigService: AppConfigService) { }
 // encrypt(value: string): string {
 //   return CryptoJS.AES.encrypt(value, this.secretKey.trim()).toString();
 // }

 // decrypt(textToDecrypt: string) {
 //   return CryptoJS.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(CryptoJS.enc.Utf8);
 // }
 // getclient(){
 //   return this.http.get<any[]>('../assets/clients.json').pipe(
 //     map((res: any[]) => res)) 
 // }  

 getAPPCli() {
   // let _url = 'https://ws-ms-baseapp-get-config-function.azurewebsites.net/api/GetConfigurations?environment=beta&expression=$';
   // return this.http.get(_url);
   return this.appConfigService.getAppConfig;
 }

 localCacheSession(key: any, value: any) {
   key = 'chache_' + key;
   localStorage.setItem(key, value);
   //const timeout:number=1000 * 60 * 60 * 24; 
   const timeout: number = 1000 * 10;
   return setTimeout(() => {
     localStorage.removeItem(key)
   }, timeout);
 }

 getprofile(company: string) {
   const useremail = localStorage.getItem('temp_email');
   const Udomain_split: string[] = useremail?.split("@") || [""];
   let doctype: string = "";
   if (Udomain_split.indexOf("wsandco.com") !== -1 || Udomain_split.indexOf("woodruffsawyer.com") !== -1) {
     doctype = "internal_users"
   } else {
     doctype = "external_users";
   }
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/query/${doctype}`;
   let body: any = {};
   body.companyName = company;
   body.email = useremail;
   return this.http.post<any[]>(url, body, { responseType: 'json' }).pipe(
     map((res: any[]) => res)
   );
 }

 getclient(company: string, isInternal: boolean) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/query/${documentName}`;
   let body: any = {};
   body.companyName = company;

   return this.http.post<ExternalUserElasticQuerySearch[]>(url, body, { responseType: 'json' }).pipe(
     map((res: ExternalUserElasticQuerySearch[]) => {
       return res.filter(x => x.source.contacts_only != true).map(u => u.source)
     })
   );

 }
 getServiceTeam(company: string, isInternal: boolean) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/query/${documentName}`;
   //console.log(url);
   let body: any = {};
   body.companyName = [];
   body.companyName.push(company);
   return this.http.post<ExternalUserElasticQuerySearch[]>(url, body, { responseType: 'json' }).pipe(
     map((res: ExternalUserElasticQuerySearch[]) => {
       return res.map(u => u.source)
     })
   );
 }

 deleteClient(email: string, isDeleted: boolean, isInternal: boolean) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${email}`;
   let body: any = {};
   body.isDeleted = isDeleted;
   return this.http.post(url, body);
 }

 updateClient(email: string, body: any = {}, isInternal: boolean) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${email}`;
   return this.http.post(url, body);
 }


 logClients(eventGridName: string, body: any = {}) {
   const url: string = `https://ms-dev-commoneventdroppingfunction.azurewebsites.net/api/common_event_dropping_lambda/${eventGridName}`
   return this.http.post(url, body);
 }

 getClientsoldone(blobName: string, body: any = {}) {
   const url: string = `https://ms-dev-activitylogsfunctions.azurewebsites.net/api/woodruff/query/activitylogs/${blobName}`
   return this.http.post(url, body);
 }

 getClients(documentName: string, body: any = {}){
  const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/UniqueValues/${documentName}`
  return this.http.post(url, body);
 }


 getUserActivity(blobName: string, paginationSize: string, startNumber: string, body: any = {}) {
   const url: string = `https://ms-dev-activitylogsfunctions.azurewebsites.net/api/woodruff/query/activitylogs/${blobName}?size=${paginationSize}&page=${startNumber}`
   return this.http.post(url, body);
 }

 uploadDocumentToBlob(target_container: string, body: any = {}, path: string, fileName: string) {
   let headers = new HttpHeaders();
   headers = headers.set('path', path);
   headers = headers.set('target_name', fileName);
   const url: string = `https://wsmsblobdata-dev-function-app.azurewebsites.net/api/upload_item/target_container/${target_container}`
   return this.http.post(url, body, { headers: headers });
 }

 saveDataToElasticSearch(document_unique_id: string, body: any = {}) {
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/ms-document-metadata/documents/${document_unique_id}`
   return this.http.post(url, body);
 }

 getDataToElasticSearch(document_unique_id: string) {
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/ms-document-metadata/documents/${document_unique_id}`
   return this.http.get(url);
 }

 getSearchData(index: string, body: any = {}) {
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/query/${index}`
   return this.http.post(url, body);

 }

 async getUserDetails(userdetailsUrl: string,) {
   const url: string = `${userdetailsUrl}`
   return await this.http.get(url).toPromise();
 }

 addBusinessClients(email: string, isInternal: boolean, body: any = {}) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/${documentName}/documents/${email}`;
   return this.http.post(url, body);
 }

 getBusinessClients(isInternal: boolean, body: any = {}) {
   const documentName: string = isInternal ? "internal_users" : "external_users";
   const url: string = `https://wsms-dev-es-function-app.azurewebsites.net/query/${documentName}`

   return this.http.post(url, body, { responseType: 'json' })
 }

 async getUserEmail(upnMail:string){
   let headers = new HttpHeaders();
   headers = headers.set('Authorization', 'Bearer '+ atob(localStorage.getItem("token") ?? ""));
   headers = headers.set('Content-Type', 'application/json');
   const url:string=`https://graph.microsoft.com/v1.0/users/${upnMail}`
    return await this.http.get(url,{ headers: headers }).toPromise();
 }

}


