import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {

    private appConfig: any;
    private configUrl: string = "";

    constructor(private http: HttpClient, private route: ActivatedRoute) {
        this.configUrl = `https://ws-ms-baseapp-get-config-function.azurewebsites.net/api/GetConfigurations?environment=${environment.environment}&expression=$`;
       
    }
    async loadAppConfig() {
        const headers = {"environment": environment.environment};
        const data = await this.http.get(this.configUrl, {headers : headers}).toPromise();
        this.appConfig = data;
        // for (const key in data) {
        //     console.log(`${key}: ${data[key]}`);
        // }
    }
    // This is an example property ... you can make it however you want.
    get getAppConfig() {
        return this.appConfig;
    }
}