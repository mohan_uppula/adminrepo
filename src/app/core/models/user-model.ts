export interface ExternalUser {
    fullName: string;
    title: string;
    Address: string;
    floor: string;
    workPhoneNumber: string;
    workPhoneCountryCode: number;
    mobilePhoneNumber: string;
    mobilePhoneCountryCode: string;
    primaryContact: string;
    email: string;
    comments: string;
    contactType: string;
    companyName: string;
    isW360Access: boolean;
    isDomestic: boolean;
    isInternational: boolean;
}
export interface ExternalUserSaveModel extends ExternalUser {
    tenantName: string;
    primayEmailId: string;
    firstName: string;
    lastName: string;
    tenantDomain: string;
    tenantPermisionRequest: string[];
    legacyPermissionRoles: LegacyPermissionRoles;
    internal_user: boolean;
    password: string;
    Domestic: string[];
    International: string[];
    isDeleted: boolean;
    contacts_only: boolean;
}
export interface LegacyPermissionRoles {
    applicationId: number;
    applicationName: string;
    subApplicationId: number;
    roleId: string;
}
export interface ContactType {
    value: string;
    text: string;
}
export interface EventGridModel {
    id: string,
    eventType: string;
    subject: string;
    eventTime: Date;
    dataVersion: string;
    metadataVersion: number;
    data: ExternalUserSaveModel;
}
export interface ExternalUserElasticSearch  {
    Values:ExternalUserSaveModel;
}
export interface ExternalUserElasticQuerySearch  {
    source:ExternalUserSaveModel;
}

//Praveen
export  interface BusinessContact{

name:string,
company:string,
phone:string,
associated_clients:string[];
clientEmail:string;
contacts_only:boolean;


}
